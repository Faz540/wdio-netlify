const expect = require("chai").expect;

describe("Simple Tests", function() { 
    describe("Product Listing Pages (PLP)", function() {
        it("expects to see products when navigating to 'All'", function() {
            browser.url("/all");
            const numberOfProducts = $$(".item").length;
            expect(numberOfProducts).to.be.greaterThan(0);
        });
        it("expects to see products when navigating to 'Women'", function() {
            browser.url("/women");
            const numberOfProducts = $$(".item").length;
            expect(numberOfProducts).to.be.greaterThan(0);
        });
        it("expects to see products when navigating to 'Men'", function() {
            browser.url("/men");
            const numberOfProducts = $$(".item").length;
            expect(numberOfProducts).to.be.greaterThan(0);
        });
        it("successfully navigates to a PDP after clicking a product image", function() {
            browser.url("/all");
            const productImages = $$(".item .img-contain a");
            productImages[0].click();
            const addToCartButton = $(".product-info .purchase");
            expect(addToCartButton).to.exist;
        });
        it("successfully navigates to a PDP after clicking the 'View Item' button", function() {
            browser.url("/all");
            const viewItemButtons = $$(".item button");
            viewItemButtons[0].click();
            const addToCartButton = $(".product-info .purchase");
            expect(addToCartButton).to.exist;
        });
        it("expects the prices displayed to have the correct currency symbol ($)", function() {
            browser.url("/all");
            const prices = $$(".item .price");
            prices.forEach(function(price) {
                expect(price.getText()).to.contain("$");
            });
        });
        it("expects the prices displayed to NOT be $0.00", function() {
            browser.url("/all");
            const pricesWithCurrencySymbols = $$(".item .price");
            const prices = pricesWithCurrencySymbols.map(function(price) {
                return parseFloat(price.getText().replace("$", "")); 
            });
            prices.forEach(function(price) {
                expect(price).to.be.greaterThan(0);
            });
        });
    });
    describe("Product Detail Pages (PDP)", function() {
        afterEach(function() {
            // Refreshing the page resets the state of the cart/basket
            browser.refresh();
        })
        it("shows '1' next to the 'Cart' menu after clicking 'Add To Cart'", function() {
            const productID = "9d436e98-1dc9-4f21-9587-76d4c0255e33";
            browser.url(`/product/${productID}`);
            const addToCartButton = $(".product-info .purchase");
            addToCartButton.click();
            const numberOfItemsInCart = $(".carttotal");
            expect(numberOfItemsInCart.getText()).to.equal("1");
        });
        it("shows '2' next to the 'Cart' menu after clicking 'Add To Cart' twice", function() {
            const productID = "9d436e98-1dc9-4f21-9587-76d4c0255e33";
            browser.url(`/product/${productID}`);
            const addToCartButton = $(".product-info .purchase");
            addToCartButton.click();
            // Sorry!
            browser.pause(500);
            addToCartButton.click();
            const numberOfItemsInCart = $(".carttotal");
            expect(numberOfItemsInCart.getText()).to.equal("2");
        });
        it("shows '2' next to the 'Cart' menu after increasing the quantity to 2 and  clicking 'Add To Cart'", function() {
            const productID = "9d436e98-1dc9-4f21-9587-76d4c0255e33";
            browser.url(`/product/${productID}`);
            const increaseQuantityButton = $$(".quantity .update-num")[1];
            increaseQuantityButton.click();
            const addToCartButton = $(".product-info .purchase");
            addToCartButton.click();
            const numberOfItemsInCart = $(".carttotal");
            expect(numberOfItemsInCart.getText()).to.equal("2");
        });
        it("shows the added product on the 'Cart' page", function() {
            const productID = "9d436e98-1dc9-4f21-9587-76d4c0255e33";
            browser.url(`/product/${productID}`);
            const addToCartButton = $(".product-info .purchase");
            addToCartButton.click();
            // Directly navigating to the cart/basket page also resets its state.
            const cartMenuLink = $("a[href='/cart']");
            cartMenuLink.click();
            // Sorry!
            browser.pause(500);
            const productsInBasket = $$("img.productimg").length;
            expect(productsInBasket).to.equal(1);
        });
    });
});