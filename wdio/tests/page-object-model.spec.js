const expect = require("chai").expect;
const plp = require("../pages/plp.page");
const pdp = require("../pages/pdp.page");
const menu = require("../pages/menu.header");
const cart = require("../pages/cart.page");

describe("Simple Tests", function() { 
    describe("Product Listing Pages (PLP)", function() {
        it("expects to see products when navigating to 'All'", function() {
            plp.open("/all")
            expect(plp.numberOfProducts()).to.be.greaterThan(0);
        });
        it("expects to see products when navigating to 'Women'", function() {
            plp.open("/women")
            expect(plp.numberOfProducts()).to.be.greaterThan(0);
        });
        it("expects to see products when navigating to 'Men'", function() {
            plp.open("/men")
            expect(plp.numberOfProducts()).to.be.greaterThan(0);
        });
        it("successfully navigates to a PDP after clicking a product image", function() {
            plp.open("/all")
            plp.clickProductImageLink(0);
            const addToCartButton = $(".product-info .purchase");
            expect(addToCartButton).to.exist;
        });
        it("successfully navigates to a PDP after clicking the 'View Item' button", function() {
            plp.open("/all")
            plp.clickViewItemTextLink(0);
            const addToCartButton = $(".product-info .purchase");
            expect(addToCartButton).to.exist;
        });
        it("expects the prices displayed to have the correct currency symbol ($)", function() {
            plp.open("/all")
            const currencySymbols = plp.getCurrencySymbols();
            currencySymbols.forEach(function(currencySymbol) {
                expect(currencySymbol).to.equal("$");
            });
        });
        it("expects the prices displayed to NOT be $0.00", function() {
            plp.open("/all")
            const prices = plp.getPrices();
            prices.forEach(function(price) {
                expect(price).to.be.greaterThan(0);
            });
        });
    });
    describe("Product Detail Pages (PDP)", function() {
        beforeEach(function() {
            const productID = "9d436e98-1dc9-4f21-9587-76d4c0255e33";
            pdp.open(productID);
        });
        afterEach(function() {
            // Refreshing the page resets the state of the cart/basket
            browser.refresh();
        });
        it("shows '1' next to the 'Cart' menu after clicking 'Add To Cart'", function() {
            pdp.addToCart();
            expect(menu.numberOfCartItems()).to.equal("1");
        });
        it("shows '2' next to the 'Cart' menu after clicking 'Add To Cart' twice", function() {
            pdp.addToCartMultipleTimes(2);
            expect(menu.numberOfCartItems()).to.equal("2");
        });
        it("shows '2' next to the 'Cart' menu after increasing the quantity to 2 and  clicking 'Add To Cart'", function() {
            pdp.increaseQuantity(2);
            pdp.addToCart();
            expect(menu.numberOfCartItems()).to.equal("2");
        });
        it("shows the added product on the 'Cart' page", function() {
            pdp.addToCart();
            // Directly navigating to the cart/basket page also resets its state.
            menu.clickMenuLink("cart");
            expect(cart.numberOfProducts()).to.equal(1);
        });
    });
});