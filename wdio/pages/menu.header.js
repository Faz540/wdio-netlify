// This is the default page. 
// All other page objects inherit from this.

class Menu {
    get itemsInCart() { return $(".carttotal"); }

    clickMenuLink(url) {
        $(`a[href='/${url}']`).click();
        // Sorry! Placeholder pause to wait for the Cart page to load
        return browser.pause(500);
    };

    numberOfCartItems() {
        return this.itemsInCart.getText();
    }

};

module.exports = new Menu();