class PLP {

    get prices() { return $$(".item .price"); }
    get productImages() { return $$(".item .img-contain a"); }
    get viewItemButtons() { return $$(".item button"); }
    get productCards() { return $$(".item"); }

    open(path) {
        return browser.url(path);
    };

    numberOfProducts() {
        return this.productCards.length;
    };

    clickProductImageLink(number) {
        return this.productImages[number].click();
    };

    clickViewItemTextLink(number) {
        return this.viewItemButtons[number].click();
    };

    getPrices() {
        return this.prices.map(function(price) {
            return parseFloat(price.getText().replace("$", ""));
        });
    };

    getCurrencySymbols() {
        return this.prices.map(function(price) {
            return price.getText()[0];
        });
    };
};

module.exports = new PLP();