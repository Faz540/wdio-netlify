class PDP {
    get addToCartButton() { return $(".product-info .purchase"); }
    get decreaseQuantityButton() { return $$(".quantity .update-num")[0]; }
    get increaseQuantityButton() { return $$(".quantity .update-num")[1]; }

    open(productID) {
        return browser.url(`/product/${productID}`) 
    };

    addToCart() {
        return this.addToCartButton.click();
    };

    addToCartMultipleTimes(numberOfIterations) {
        let i = 0;
        while(i < numberOfIterations) {
            this.addToCart();
            // Delay between clicks
            browser.pause(100);
            i++;
        };
    };

    increaseQuantity(quantity) {
        let i = 1;
        while(i < quantity) {
            this.increaseQuantityButton.click();
            // Delay between clicks
            browser.pause(100);
            i++;
        };
    };
};

module.exports = new PDP();