class Cart {
    get productsInBasket() { return $$("img.productimg"); }

    numberOfProducts() {
        return this.productsInBasket.length;
    };
};



module.exports = new Cart();