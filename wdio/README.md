```
yarn install
```

```
yarn build
```

Once built, run the below command:
```
yarn start
```

You'll be given an address to start testing against, it should say something like: 
	"Listening on: http://localhost:3000/"
	Open Google Chrome and navigate to the URL: "http://localhost:3000"
	You should now see a local version of the ecommerce website! 

Before writing any automated tests, I'd strongly suggest to manually test the website to understand how the application (the website) behavaiour.

To run the tests locally, run the below command:
```
yarn test:dev
```

To run the tests against the production environment, run the below command:
```
test:production
```

Known Issues:
- The "all" and "women" PLPs return a 404 when navigating to them for the first time after starting the local server. Once this issue has been encountered, all subsequent tests run fine.
- Directly navigating to the "all" or "men" PLPs on the production environment, return a 404. The menu links work fine. This can be replicated by navigating to these pages via the Menu links and then refreshing the page. Author has been notified.