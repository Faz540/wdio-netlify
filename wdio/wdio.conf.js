exports.config = {
    baseUrl: 'http://localhost:3000',
    runner: 'local',
    path: '/',
    specs: [
        // 'wdio/tests/simple.spec.js',
        'wdio/tests/page-object-model.spec.js'
    ],
    exclude: [
        // 'path/to/excluded/files'
    ],
    maxInstances: 1,
    capabilities: [{
        maxInstances: 1,
        browserName: 'chrome',
    }],
    logLevel: 'error',
    // Default timeout for all waitFor* commands.
    waitforTimeout: 10000,
    // Test runner services
    // Services take over a specific job you don't want to take care of. They enhance
    // your test setup with almost no effort. Unlike plugins, they don't add new
    // commands. Instead, they hook themselves up into the test process.
    services: ['chromedriver'],
    framework: 'mocha',
    reporters: ['spec'],
    // Options to be passed to Mocha.
    // See the full list at http://mochajs.org/
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000
    },
    afterTest: function(test) {
        if(!test.passed)  {
            browser.saveScreenshot(`./wdio/screenshots/${test.fullTitle}.png`);
        } 
    }
}